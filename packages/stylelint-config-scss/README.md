# Personal configs

Credit:  Forked from [ntnyq](https://github.com/ntnyq/configs)

# StyleLint config for SCSS

## Usage

### Install

```bash
$ npm i @pinnsg/stylelint-config-scss -D
# OR
$ yarn add @pinnsg/stylelint-config-scss -D
```

### Extend this config

in `.stylelintrc.yml`

```yaml
extends:
  - @pinnsg/stylelint-config-scss
```

in `package.json`

```json
{
    "stylelint": {
        "extends": ["@pinnsg/stylelint-config-scss"]
    }
}
```
