/**
 * React.js ESLint Rules with Prettier
 */

module.exports = {
  extends: ["@pinnsg/react", "@pinnsg/prettier"],
};
