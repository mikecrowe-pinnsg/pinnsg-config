# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.3.0](https://github.com/pinnsg/configs/compare/@pinnsg/configs-typescript@0.2.1...@pinnsg/configs-typescript@0.3.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.2.0](https://github.com/pinnsg/configs/compare/@pinnsg/configs-typescript@0.2.1...@pinnsg/configs-typescript@0.2.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





## [0.1.3](https://github.com/pinnsg/configs/compare/@pinnsg/configs-typescript@0.1.2...@pinnsg/configs-typescript@0.1.3) (2021-07-27)

**Note:** Version bump only for package @pinnsg/configs-typescript

## [0.1.2](https://github.com/pinnsg/configs/compare/@pinnsg/configs-typescript@0.1.1...@pinnsg/configs-typescript@0.1.2) (2021-07-27)

**Note:** Version bump only for package @pinnsg/configs-typescript

## 0.1.1 (2021-07-10)

**Note:** Version bump only for package @pinnsg/configs-typescript
