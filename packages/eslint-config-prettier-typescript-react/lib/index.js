/**
 * Vue.js Rules with Typescript Rules with Prettier
 */

module.exports = {
  extends: [
    "@pinnsg/prettier-typescript",
    "@pinnsg/prettier-react",
    "@pinnsg/typescript-react",
  ],
};
