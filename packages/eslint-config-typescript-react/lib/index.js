/**
 * React.js Rules with Typescript Rules
 */

module.exports = {
  extends: ["@pinnsg/typescript", "@pinnsg/react"],
};
