# Personal configs

Credit:  Forked from [ntnyq](https://github.com/ntnyq/configs)

# Prettier config

## Usage

### Install

```bash
$ npm i @pinnsg/prettier-config -D
# OR
$ yarn add @pinnsg/prettier-config -D
```

### Extend this config

in `package.json`

```json
{
    "prettier": "@pinnsg/prettier-config"
}
```
