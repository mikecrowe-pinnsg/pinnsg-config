# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.4.1](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.4.0...@pinnsg/prettier-config@0.4.1) (2022-05-14)

**Note:** Version bump only for package @pinnsg/prettier-config





# [0.4.0](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.2.1...@pinnsg/prettier-config@0.4.0) (2021-07-28)


### Bug Fixes

* standardizing ([06fbc61](https://github.com/pinnsg/configs/commit/06fbc61019571a0ecf17298f0121f2cdc9b379e9))


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.3.0](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.2.1...@pinnsg/prettier-config@0.3.0) (2021-07-28)


### Bug Fixes

* standardizing ([06fbc61](https://github.com/pinnsg/configs/commit/06fbc61019571a0ecf17298f0121f2cdc9b379e9))


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.2.0](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.1.3...@pinnsg/prettier-config@0.2.0) (2021-07-28)


### Features

* general package/doc cleanup ([c69a8f6](https://github.com/pinnsg/configs/commit/c69a8f60a03531f44d7996955d48d522d9637427))





## [0.1.3](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.1.2...@pinnsg/prettier-config@0.1.3) (2021-07-27)

**Note:** Version bump only for package @pinnsg/prettier-config

## [0.1.2](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.1.1...@pinnsg/prettier-config@0.1.2) (2021-07-27)

**Note:** Version bump only for package @pinnsg/prettier-config

## 0.1.1 (2021-07-10)

**Note:** Version bump only for package @pinnsg/prettier-config

# [0.3.0](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.2.1...@pinnsg/prettier-config@0.3.0) (2021-01-30)

### Features

- **prettier-config:** tweak prettier config ([bc8a347](https://github.com/pinnsg/configs/commit/bc8a3477e92a3c8e122816e000d2cdb64e6d5009))

## [0.2.1](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.2.0...@pinnsg/prettier-config@0.2.1) (2021-01-30)

**Note:** Version bump only for package @pinnsg/prettier-config

# [0.2.0](https://github.com/pinnsg/configs/compare/@pinnsg/prettier-config@0.1.0...@pinnsg/prettier-config@0.2.0) (2020-11-19)

### Features

- extract babel-eslint ([a1f7446](https://github.com/pinnsg/configs/commit/a1f744685ff7038a72a94a0efe69b28eb27d0a7e))
- prettier overrides config ([3185cbf](https://github.com/pinnsg/configs/commit/3185cbf4a167796c4a702e7bc76a8193e5596551))

# 0.1.0 (2020-09-12)

### Features

- add options ([fa49d8c](https://github.com/pinnsg/configs/commit/fa49d8cb38e86bc8f6be3825d0b0bd647ae474b1))
