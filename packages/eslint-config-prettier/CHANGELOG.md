# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.4.3](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.4.2...@pinnsg/eslint-config-prettier@0.4.3) (2022-05-14)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier





## [0.4.2](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.4.1...@pinnsg/eslint-config-prettier@0.4.2) (2022-05-14)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier





## [0.4.1](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.4.0...@pinnsg/eslint-config-prettier@0.4.1) (2021-09-06)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier





# [0.4.0](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.2.2...@pinnsg/eslint-config-prettier@0.4.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.3.0](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.2.2...@pinnsg/eslint-config-prettier@0.3.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.2.0](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.4...@pinnsg/eslint-config-prettier@0.2.0) (2021-07-28)


### Features

* general package/doc cleanup ([c69a8f6](https://github.com/pinnsg/configs/commit/c69a8f60a03531f44d7996955d48d522d9637427))





## [0.1.4](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.2...@pinnsg/eslint-config-prettier@0.1.4) (2021-07-27)

### Bug Fixes

- fixing typescript config ([90388c4](https://github.com/pinnsg/configs/commit/90388c4a744ba11070f668e752123d549994c4fb))

## [0.1.3](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.2...@pinnsg/eslint-config-prettier@0.1.3) (2021-07-27)

### Bug Fixes

- fixing typescript config ([90388c4](https://github.com/pinnsg/configs/commit/90388c4a744ba11070f668e752123d549994c4fb))

## [0.1.2](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.1...@pinnsg/eslint-config-prettier@0.1.2) (2021-07-12)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier

## 0.1.1 (2021-07-10)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier

## [0.3.2](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.3.1...@pinnsg/eslint-config-prettier@0.3.2) (2021-02-20)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier

## [0.3.1](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.3.0...@pinnsg/eslint-config-prettier@0.3.1) (2021-01-30)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier

# [0.3.0](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.2.1...@pinnsg/eslint-config-prettier@0.3.0) (2020-12-10)

### Features

- deprecate eslint-plugin-standard ([66f0e1a](https://github.com/pinnsg/configs/commit/66f0e1a2ca5060a631477a69d6706a6a8fda2708))

## [0.2.1](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.2.0...@pinnsg/eslint-config-prettier@0.2.1) (2020-11-19)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier

# [0.2.0](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.3...@pinnsg/eslint-config-prettier@0.2.0) (2020-11-07)

### Features

- extract babel-eslint ([a1f7446](https://github.com/pinnsg/configs/commit/a1f744685ff7038a72a94a0efe69b28eb27d0a7e))

## [0.1.3](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.2...@pinnsg/eslint-config-prettier@0.1.3) (2020-10-10)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier

## [0.1.2](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.1...@pinnsg/eslint-config-prettier@0.1.2) (2020-09-23)

**Note:** Version bump only for package @pinnsg/eslint-config-prettier

## [0.1.1](https://github.com/pinnsg/configs/compare/@pinnsg/eslint-config-prettier@0.1.0...@pinnsg/eslint-config-prettier@0.1.1) (2020-09-12)

### Bug Fixes

- invalid file name ([83a0c9c](https://github.com/pinnsg/configs/commit/83a0c9c119b2fb36a538948b2ba524caafe6fd9e))

# 0.1.0 (2020-09-12)

### Features

- add presets ([6fe94fa](https://github.com/pinnsg/configs/commit/6fe94fae4ed9d80b18833c9e5a3f51f710ebda43))
