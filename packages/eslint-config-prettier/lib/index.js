/**
 * Standard JavaScript Style with Prettier
 */

module.exports = {
  extends: [
    "@pinnsg",
    // Extends preset about prettier
    require.resolve("./prettier"),
  ],
};
