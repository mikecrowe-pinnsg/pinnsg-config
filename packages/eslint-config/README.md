# Personal configs

Credit:  Forked from [ntnyq](https://github.com/ntnyq/configs)

# EsLint config

## Usage

### Install

```bash
$ npm i @pinnsg/eslint-config -D
# OR
$ yarn add @pinnsg/eslint-config -D
```

### Extend this config

in `.eslintrc.js`

```js
module.exports = {
    root: true,

    extends: ['@pinnsg'],

    rules: {
        // Override rules
    },
}
```

in `package.json`

```json
{
    "eslintConfig": {
        "root": true,
        "extends": "@pinnsg"
    }
}
```
