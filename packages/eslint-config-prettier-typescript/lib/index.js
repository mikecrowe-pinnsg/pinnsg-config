/**
 * TypeScript ESLint Rules with Prettier
 */

module.exports = {
  extends: ["@pinnsg/typescript", "@pinnsg/prettier"],
};
