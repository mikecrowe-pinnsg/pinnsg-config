/**
 * Vue.js Rules and Typescript Rules with Prettier
 */

module.exports = {
  extends: [
    "@pinnsg/prettier-vue",
    "@pinnsg/prettier-typescript",
    "@pinnsg/typescript-vue",
  ],
};
