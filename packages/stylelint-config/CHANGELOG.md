# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.4.1](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.4.0...@pinnsg/stylelint-config@0.4.1) (2022-05-14)

**Note:** Version bump only for package @pinnsg/stylelint-config





# [0.4.0](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.2.2...@pinnsg/stylelint-config@0.4.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.3.0](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.2.2...@pinnsg/stylelint-config@0.3.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.2.0](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.1.3...@pinnsg/stylelint-config@0.2.0) (2021-07-28)


### Features

* general package/doc cleanup ([c69a8f6](https://github.com/pinnsg/configs/commit/c69a8f60a03531f44d7996955d48d522d9637427))





## [0.1.3](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.1.1...@pinnsg/stylelint-config@0.1.3) (2021-07-27)

### Bug Fixes

- fixing typescript config ([90388c4](https://github.com/pinnsg/configs/commit/90388c4a744ba11070f668e752123d549994c4fb))

## [0.1.2](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.1.1...@pinnsg/stylelint-config@0.1.2) (2021-07-27)

### Bug Fixes

- fixing typescript config ([90388c4](https://github.com/pinnsg/configs/commit/90388c4a744ba11070f668e752123d549994c4fb))

## 0.1.1 (2021-07-10)

**Note:** Version bump only for package @pinnsg/stylelint-config

## [0.1.2](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.1.1...@pinnsg/stylelint-config@0.1.2) (2021-01-30)

**Note:** Version bump only for package @pinnsg/stylelint-config

## [0.1.1](https://github.com/pinnsg/configs/compare/@pinnsg/stylelint-config@0.1.0...@pinnsg/stylelint-config@0.1.1) (2020-09-14)

### Bug Fixes

- stylelint ignore files ([9cedec2](https://github.com/pinnsg/configs/commit/9cedec230186d4d1cdbb6a02188c1bd8baf4c00e))

# 0.1.0 (2020-09-14)

### Features

- stylelint-config ([2c2310e](https://github.com/pinnsg/configs/commit/2c2310efbdb36e9eb00b778f0eeb09054aa6fd1d))
