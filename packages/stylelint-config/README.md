# Personal configs

Credit:  Forked from [ntnyq](https://github.com/ntnyq/configs)

# StyleLint config

## Usage

### Install

```bash
$ npm i @pinnsg/stylelint-config -D
# OR
$ yarn add @pinnsg/stylelint-config -D
```

### Extend this config

in `.stylelintrc.yml`

```yaml
extends:
  - @pinnsg/stylelint-config
```

in `package.json`

```json
{
    "stylelint": {
        "extends": ["@pinnsg/stylelint-config"]
    }
}
```
