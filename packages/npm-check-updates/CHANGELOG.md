# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.4.2](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.4.1...@pinnsg/config-npm-check-updates@0.4.2) (2021-09-06)

**Note:** Version bump only for package @pinnsg/config-npm-check-updates





## [0.4.1](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.4.0...@pinnsg/config-npm-check-updates@0.4.1) (2021-07-29)

**Note:** Version bump only for package @pinnsg/config-npm-check-updates





# [0.4.0](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.2.1...@pinnsg/config-npm-check-updates@0.4.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.3.0](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.2.1...@pinnsg/config-npm-check-updates@0.3.0) (2021-07-28)


### Features

* resetting and testing ([f925cc7](https://github.com/pinnsg/configs/commit/f925cc782e7354678110bdf80a2ebb12b31365df))





# [0.2.0](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.1.5...@pinnsg/config-npm-check-updates@0.2.0) (2021-07-28)


### Features

* general package/doc cleanup ([c69a8f6](https://github.com/pinnsg/configs/commit/c69a8f60a03531f44d7996955d48d522d9637427))





## [0.1.5](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.1.4...@pinnsg/config-npm-check-updates@0.1.5) (2021-07-27)

**Note:** Version bump only for package @pinnsg/config-npm-check-updates

## [0.1.4](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.1.3...@pinnsg/config-npm-check-updates@0.1.4) (2021-07-27)

**Note:** Version bump only for package @pinnsg/config-npm-check-updates

## [0.1.3](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.1.1...@pinnsg/config-npm-check-updates@0.1.3) (2021-07-12)

**Note:** Version bump only for package @pinnsg/config-npm-check-updates

## [0.1.2](https://github.com/pinnsg/configs/compare/@pinnsg/config-npm-check-updates@0.1.1...@pinnsg/config-npm-check-updates@0.1.2) (2021-07-12)

**Note:** Version bump only for package @pinnsg/config-npm-check-updates

## 0.1.1 (2021-07-10)

**Note:** Version bump only for package @pinnsg/config-npm-check-updates
