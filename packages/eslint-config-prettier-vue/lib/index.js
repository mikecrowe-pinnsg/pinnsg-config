/**
 * Vue.js ESLint Rules with Prettier
 */

module.exports = {
  extends: ["@pinnsg/vue", "@pinnsg/prettier"],
};
